use mysql;

delimiter |
drop procedure if exists system_info_exists;
create procedure system_info_exists ()
begin
    declare schema_exist int;
    select if (exists (select schema_name from information_schema.schemata where schema_name = 'system_info'), 1, 0) into schema_exist;
    if schema_exist = 0 then
        create database system_info;

        create table if not exists system_info.modules (
            module varchar(64) not null,
            version varchar(8) not null,
            primary key (module, version)
        );

        insert into system_info.modules (module, version)
        values ('system-info', '1.0.0')
        on duplicate key update module = module;
    end if;
end|
delimiter ;

call system_info_exists();
drop procedure if exists system_info_exists;
