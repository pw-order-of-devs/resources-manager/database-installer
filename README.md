# Database Installer

Scripts for initialize data base schemas

Currently supported schemas:
- postgesql
  - uses bytea type to store large objects
- postgresql-fs
  - stores files in filesystem
  - file path is stored in database
