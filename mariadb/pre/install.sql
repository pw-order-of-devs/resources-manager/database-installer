delimiter |
begin not atomic
    if not exists(select 1 from information_schema.schemata where schema_name='system_info') then
        create database system_info;

        create table if not exists system_info.modules (
           module varchar(64) not null,
           version varchar(8) not null,
           primary key (module, version)
        );

        insert into system_info.modules (module, version)
        values ('system-info', '1.0.0')
        on duplicate key update module = module;
    end if;
end|
delimiter ;
