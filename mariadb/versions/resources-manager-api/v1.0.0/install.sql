delimiter |
begin not atomic
    if not exists(select 1 from system_info.modules where module='resources-manager-api') then
        create database if not exists resource_provider;

        create table if not exists resource_provider.resources (
           id char(36) primary key not null,
           modification_date bigint,
           creation_date bigint not null,
           content longblob not null,
           file_type text check (file_type in ('audio', 'text', 'video')),
           name text not null,
           description text,
           author_id text,
           size bigint,
           duration bigint,
           tags text,
           time_pinpoints text
        );

        insert into system_info.modules (module, version)
        values ('resources-manager-api', '1.0.0')
        on duplicate key update module = module;
    end if;
end|
delimiter ;
