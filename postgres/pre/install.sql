do $$
begin
    if not exists (select 1 from pg_namespace where nspname = 'system_info') then
        create schema if not exists system_info;

        create table if not exists system_info.modules (
           module text not null,
           version text not null,
           primary key (module, version)
        );

        insert into system_info.modules (module, version)
        values ('system-info', '1.0.0')
        on conflict do nothing;
    end if;
end
$$;
