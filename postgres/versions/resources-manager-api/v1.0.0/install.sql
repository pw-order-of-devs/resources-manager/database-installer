do $$
begin
    if not exists (select 1 from system_info.modules where module = 'resources-manager-api' and version = '1.0.0') then
        create schema if not exists resource_provider;

        if not exists (
                select 1 from pg_type
                where typname = 'time_pinpoint'
                  and typnamespace = (select oid from pg_namespace where nspname = 'resource_provider')) then
            create type resource_provider.time_pinpoint as (
               moment numeric,
               description text
           );
        end if;

        create table if not exists resource_provider.resources (
           id char(36) primary key not null,
           modification_date numeric,
           creation_date numeric not null,
           content bytea not null,
           file_type text check (file_type in ('audio', 'text', 'video')),
           name text not null,
           description text,
           author_id text,
           size numeric,
           duration numeric,
           tags text[],
           time_pinpoints resource_provider.time_pinpoint[]
        );

        insert into system_info.modules (module, version)
        values ('resources-manager-api', '1.0.0')
        on conflict do nothing;
    end if;
end
$$;
