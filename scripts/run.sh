#!/bin/bash

echo "Preparing DB: ${DB_HOST}:${DB_PORT}/${DB_DATABASE}"

workdir="$(pwd)"

while true; do
  ("${workdir}"/scripts/try_connect.sh > /dev/null)
  ret=$?
  if [ $ret -ne 0 ]; then
    sleep 3
  else
    break
  fi
done

echo "installing prerequisites"
"${workdir}"/scripts/pre_install.sh "$(pwd)/pre/install.sql"

modules=('resources-manager-api')
for module in "${modules[@]}"; do
  echo "installing module: ${module}"
  cd "${workdir}/versions/${module}" || return
  versions=$(ls -d ./*)
  for version in "${versions[@]}"; do
    echo ">> installing version: ${version}"
    cd "${workdir}/versions/${module}/$version" || return
    "${workdir}"/scripts/install.sh
  done
done

echo "done"
