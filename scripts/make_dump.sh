#!/bin/bash

workdir=$(pwd)
output="${workdir}/${1-sql_dump.sql}"

echo "-- prerequisites" >> "$output"
cat "${workdir}/pre/install.sql" >> "$output"

modules=('resources-manager-api')
for module in "${modules[@]}"; do
  cd "${workdir}/versions/${module}" || return
  versions=$(ls -d ./*)
  for version in "${versions[@]}"; do
    cd "${workdir}/versions/${module}/$version" || return
    echo "-- ${module} ${version}" >> "$output"
    cat install.sql >> "$output"
  done
done
